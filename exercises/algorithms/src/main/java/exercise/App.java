package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] sort(int[] arr) {
        int length = arr.length;
        if (length == 0) return arr;

        for (int i = length - 1; i > 0; i--) {
            int indexOfMaxElement =
                    findIndexOfMaxElement(arr, (i + 1));

            if (indexOfMaxElement != i) {
                int temp = arr[indexOfMaxElement];
                arr[indexOfMaxElement] = arr[i];
                arr[i] = temp;
            }
        }

        return arr;
    }


    static int findIndexOfMaxElement(int[] arr, int lastIndex) {
        int length = arr.length;
        if (length == 0) return -1;
        if (length == 1) return 0;

        int maxElement = arr[0];
        int indexOfMaxElement = 0;

        for(int i = 1; i < lastIndex; i ++) {
            if (arr[i] > maxElement) {
                maxElement = arr[i];
                indexOfMaxElement = i;
            }
        }

        return indexOfMaxElement;
    }

    // // Сортировка пузырьком

    //    static int[] sort(int[] arr) {
    //        int length = arr.length;
    //        if (length == 0) return arr;
    //
    //        for (int i = 1; i < length; i++) {
    //            for (int j = 1; j <= (length - i); j += 1) {
    //                if (arr[j - 1] > arr[j]){
    //                    int temp = arr[j - 1];
    //                    arr[j - 1] = arr[j];
    //                    arr[j] = temp;
    //                }
    //            }
    //        }
    //        return arr;
    //    }

    // END
}
