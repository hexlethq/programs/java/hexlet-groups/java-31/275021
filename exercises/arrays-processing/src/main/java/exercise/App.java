package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegativeVar2(int[] arr) {
        int length = arr.length;
        if(length == 0) return -1;

        double maxMinElement = -1.0 / 0;
        int result = -1;

        for (int i = 0; i < length; i += 1) {
            if (arr[i] < 0 && arr[i] > maxMinElement) {
                maxMinElement = arr[i];
                result = i;
            }
        }
        return result;

    }

    public static int getIndexOfMaxNegative(int[] arr) {
        int length = arr.length;
        if(length == 0) return -1;

        int range = 0;
        int[] negativeNumbers = filterLessThen(arr, range);

        if(negativeNumbers.length == 0) return -1;

        int maxNegativeElement = getMaxElement(negativeNumbers);
        int result = getFirstIndexOfElement(arr, maxNegativeElement);
        return result;
    }

    static int[] getElementsLessAverage(int[] arr) {
        if (arr.length == 0) return arr;
        int average = getAverage(arr);
        return filterLessThen(arr, average);
    }

    // Самостоятельная работа

    public static int getSumBeforeMinAndMax(int[] arr) {
        if (arr.length == 0) return 0;

        int minElement = getMinElement(arr);
        int indexOfMinElement = getFirstIndexOfElement(arr, minElement);
        int maxElement = getMaxElement(arr);
        int indexOfMaxElement = getLastIndexOfElement(arr, maxElement);

        int[] interval = Arrays.copyOfRange(arr, Math.min(indexOfMinElement, indexOfMaxElement) + 1,
                Math.max(indexOfMinElement, indexOfMaxElement));
        if (interval.length < 2) return 0;

        return getSumOfArray(interval);
    }

    // Вспомогательные методы

    static int[] filterLessThen(int[] arr, int range) {
        int counter = 0;
        int index = 0;
        int[] arrResult;

        for (int element : arr) {
            if (element < range) counter += 1;
        }

        if (counter == 0) return new int[]{};

        arrResult = new int[counter];

        for(int element : arr) {
            if (element < range) {
                arrResult[index] = element;
                index += 1;
            }
        }
        return arrResult;
    }

    static int getMaxElement(int[] arr) {
        int maxElement = arr[0];

        for (int i = 0; i < arr.length; i += 1) {
            if (arr[i] > maxElement) {
                maxElement = arr[i];
            }
        }
        return maxElement;
    }

    static int getMinElement(int[] arr) {
        int minElement = arr[0];

        for (int i = 0; i < arr.length; i += 1) {
            if (arr[i] < minElement) {
                minElement = arr[i];
            }
        }
        return minElement;
    }

    static int getFirstIndexOfElement(int[] arr, int element) {
        for (int i = 0; i < arr.length; i += 1) {
            if (arr[i] == element) return i;
        }
        return -1;
    }

    static int getLastIndexOfElement(int[] arr, int element) {
        for (int i = arr.length - 1; i >= 0 ; i -= 1) {
            if (arr[i] == element) return i;
        }
        return -1;
    }

    static int getAverage (int[] arr) {
        int sumOfElements = 0;
        for (int element : arr) sumOfElements += element;

        int result = (int) Math.ceil(1.0 * sumOfElements / arr.length);
        return result;
    }

    static int getSumOfArray(int[] arr) {
        int length = arr.length;
        if (length == 0) return 0;
        int sum = 0;

        for (int current : arr) {
            sum += current;
        }
        return sum;
    }
    // END
}
