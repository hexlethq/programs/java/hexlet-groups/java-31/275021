package exercise;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

class App {
    // BEGIN
    public static String buildList(String[] list) {
        if (list.length == 0) return "";

        StringBuilder sb = new StringBuilder();
        sb.append("<ul>\n");

        for (String current : list) {
            sb.append("  <li>").append(current).append("</li>\n");
        }

        return sb.append("</ul>").toString();
    }

    public static String getUsersByYear(String[][] users, int year) {
        if (users.length == 0) return "";
        boolean flag = true;
        StringBuilder sb = new StringBuilder();
        sb.append("<ul>\n");

        for (String[] current : users) {
            LocalDate birthday = LocalDate.parse(current[1]);

            if (birthday.getYear() == year) {
                sb.append("  <li>").append(current[0]).append("</li>\n");
                flag = false;
            }
        }

        if (flag) return "";
        return sb.append("</ul>").toString();
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        int length = users.length;
        if (length == 0) return "";

        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("d MMM yyyy");
        LocalDate comparedDate = LocalDate.parse(date, formatter2);
        String[]  youngestFromSelected = {"", "0001-01-01"};

        for (String[] current : users) {
            LocalDate birthday = LocalDate.parse(current[1], formatter1);

            if (birthday.isBefore(comparedDate)
                && birthday.isAfter(LocalDate.parse(youngestFromSelected[1], formatter1))) {
                youngestFromSelected[0] = current[0];
                youngestFromSelected[1] = current[1];
            }
        }

        if (youngestFromSelected[0] == "") return "";
        return youngestFromSelected[0];
        // END
    }
}
