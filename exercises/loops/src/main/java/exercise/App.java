package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str) {
        int length = str.length();
        String result = String.valueOf(str.charAt(0)).toUpperCase();

        for (int i = 1; i < length - 1; i +=1) {
            if (str.charAt(i) == ' ') {
                result += String.valueOf(str.charAt(i + 1)).toUpperCase();
            }
        }

        return result;
    }
    // END
}
