package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String lastDigits = cardNumber.substring(12);
        String delimeter = "*";
        String hiddenDigits = "";
        String formatedCardNumber;

        for (int i = 0; i < starsCount; i += 1) {
            hiddenDigits += delimeter;
        }

        formatedCardNumber = String.format("%s%s", hiddenDigits, lastDigits);
        System.out.println(formatedCardNumber);
        // END
    }
}
