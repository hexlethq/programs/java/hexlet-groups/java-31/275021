package exercise;

import java.util.Locale;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        char lastSign = sentence.charAt(sentence.length() -1);

        String transformedSentence = lastSign == '!' ? sentence.toUpperCase() : sentence.toLowerCase();

        System.out.println(transformedSentence);
        // END
    }
}
