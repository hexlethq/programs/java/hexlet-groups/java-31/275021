package exercise;

class Converter {
    // BEGIN
    public static int convert(int baseNumber, String direction) {
        int result;

        switch (direction) {
            case ("b"):
                result = baseNumber * 1024;
                break;
            case ("Kb"):
                result = baseNumber / 1024;
                break;
            default:
                result = 0;
        }
        return result;
    }

    public static void main (String[] args) {
        int kilobyteNumber = 10;
        String direction = "b";
        int byteNumber = convert(kilobyteNumber, direction);
        String result = String.format("%s Kb = %s b", kilobyteNumber, byteNumber);
        System.out.println(result);
    }
    // END
}
