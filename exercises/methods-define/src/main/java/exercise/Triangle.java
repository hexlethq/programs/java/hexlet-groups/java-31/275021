package exercise;

class Triangle {
    // BEGIN
    public static double getSquare (double firstSideLength, double secondSideLength, double angleGrad) {
        double angelRad = angleGrad * Math.PI / 180;
        double square = Math.sin(angelRad) * firstSideLength * secondSideLength / 2;

        return square;
    }

    public static void main(String[] args) {
        double a = 4.0;
        double b = 5.0;
        double alfa= 45.0;

        System.out.println(getSquare(a, b, alfa));
    }
    // END
}
