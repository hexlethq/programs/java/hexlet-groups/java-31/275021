package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {
        if (a <= 0 || b <= 0 || c <= 0) {
            return "Треугольник не существует";
        }

        if ((a + b) < c || (a + c) < b || (b + c) < a) {
            return "Треугольник не существует";
        }

        if (a == b & a == c & b == c ) {
            return "Равносторонний";

        }if (a == b || a == c || b == c ) {
            return "Равнобедренный"; //
        }

        return "Разносторонний";
    }
    // END
}
