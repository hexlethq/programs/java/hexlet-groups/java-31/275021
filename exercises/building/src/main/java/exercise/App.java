// BEGIN
package exercise;

import com.google.gson.*;
public class App {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }

    public String toJson(String[] arr) {
        Gson gson = new Gson();
        String json = gson.toJson(arr);
        return json;
    }

}


// END
