package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] arr) {
        int length = arr.length;

        if (length == 0) {
            return arr;
        }

        int[] result = new int [length];

        for (int i = 0; i < length; i += 1) {
            result[length - i - 1] = arr[i];
        }
        return result;
    }

    public static int mult(int [] arr) {
        int length = arr.length;

        int result = 1;

        for (int i = 0; i < length; i += 1) {
            result *= arr[i];
        }

        return result;
    }

    public static int[] flattenMatrix(int[][] matrix) {
        int rowsNumber = matrix.length;

        if (rowsNumber == 0) {
            return new int[0];
        }

        int columnsNumber = matrix[0].length;
        int arrLength = rowsNumber * columnsNumber;
        int[] arr = new int[arrLength];
        int index = 0;

        for (int i = 0; i < rowsNumber; i += 1) {
                for (int j = 0; j < columnsNumber; j += 1) {
                    arr[index] = matrix[i][j];
            }
        }

        return arr;
    }
    // END
}
