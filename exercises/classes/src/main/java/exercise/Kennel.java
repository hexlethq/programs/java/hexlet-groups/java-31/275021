package exercise;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

// BEGIN
public class Kennel {
    private static ArrayList<String[]> Puppies = new ArrayList<>();
    public static void addPuppy(String[] newPuppy) {
        Puppies.add(newPuppy);
    }

    public static void addSomePuppies(String[][] newPuppies){
        if (newPuppies.length != 0) {
            for (String[] current : newPuppies) {
                addPuppy(current);
            }
        }
    }

    public static int getPuppyCount() {
        return Puppies.size();
    }

    public static boolean isContainPuppy(String name) {
        ListIterator<String[]> iter = Puppies.listIterator();

        while (iter.hasNext()) {
            String[] currentPuppi = iter.next();
            if (currentPuppi[0] == name) return true;
        }

        return false;
    }

    public static String[][] getAllPuppies() {
        return Puppies.toArray(new String[0][]); // IDEA предложила такой вариант,
        // мне пока не понятно приведение типа ArrayList<String[]> к String[][]
    }

    public static void resetKennel() {
        Puppies.clear();
    }

    public static String[] getNamesByBreed(String breed) {
        ArrayList<String> NamesByBreed = new ArrayList<>();
        ListIterator<String[]> iterator = Puppies.listIterator();

        while (iterator.hasNext()) {
            String[] currentPuppi = iterator.next();
            if (currentPuppi[1] == breed) {
                NamesByBreed.add(currentPuppi[0]);
            }
        }

        return NamesByBreed.toArray(new String[0]);
    }

    public static boolean removePuppy(String name) {
        ListIterator<String[]> iterator = Puppies.listIterator();

        while(iterator.hasNext()){
            String[] currentPuppy = iterator.next();
            if (currentPuppy[0].equals(name)) {
                Puppies.remove(currentPuppy);
                return true;
            }
        }
        return false;
    }
}

// END
