package exercise;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.assertj.core.api.Assertions.assertThat;

class AppTest {

    @BeforeEach
    void clearStaticVariable() throws Exception {
        Kennel.resetKennel();
    }

    @Test
    void testKennel() {
        Kennel.addPuppy(new String[] {"Rex", "boxer"});
        assertThat(Kennel.getPuppyCount()).isEqualTo(1);

        String[][] puppies = {
            {"Rocky", "terrier"},
            {"Buddy", "chihuahua"},
            {"Toby", "chihuahua"},
        };

        Kennel.addSomePuppies(puppies);
        assertThat(Kennel.getPuppyCount()).isEqualTo(4);
        assertThat(Kennel.isContainPuppy("Buddy")).isTrue();
        assertThat(Kennel.isContainPuppy("Olson")).isFalse();

        String[][] actual1 = Kennel.getAllPuppies();
        String[][] expected1 = {
            {"Rex", "boxer"},
            {"Rocky", "terrier"},
            {"Buddy", "chihuahua"},
            {"Toby", "chihuahua"},
        };
        assertThat(actual1).isDeepEqualTo(expected1);

        String[] actual2 = Kennel.getNamesByBreed("chihuahua");
        assertThat(actual2).containsOnly("Buddy", "Toby");


        // Тест к самостоятельной работе
        Kennel.resetKennel();
        assertThat(Kennel.getPuppyCount()).isEqualTo(0);

        Kennel.resetKennel();
        String[][] puppies2 = {
                {"Teddy", "dog"},
                {"Lessie", "doberman"}
        };
        Kennel.addSomePuppies(puppies2);
        assertThat(Kennel.removePuppy("Jack")).isFalse();
        assertThat(Kennel.removePuppy("Lessie")).isTrue();
        String[][] actual3 = Kennel.getAllPuppies();
        String[][] expected3 = {
                {"Teddy", "dog"},
        };
        assertThat(actual3).isEqualTo(expected3);
    }

    // BEGIN
    
    // END
}
