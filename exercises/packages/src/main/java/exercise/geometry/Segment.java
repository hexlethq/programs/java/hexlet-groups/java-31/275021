// BEGIN
package exercise.geometry;

public class Segment {
    public static double[][] makeSegment(double[] begin, double[] end) {
        double[][] segment = {{begin[0], begin[1]}, {end[0], end[1]}};
        return segment;
    }

    public static double[] getBeginPoint(double[][] segment) {
        double[] beginPoint = {segment[0][0], segment[0][1]};
        return beginPoint;
    }

    public static double[] getEndPoint(double[][] segment) {
        double[] endPoint = {segment[1][0], segment[1][1]};
        return endPoint;
    }
}

// END
