// BEGIN
package exercise;
import exercise.geometry.*;

public class App {
    public static double[] getMidpointOfSegment(double[][] segment) {
        double midX = segment[0][0] + (segment[1][0] - segment[0][0]) / 2;
        double midY = segment[0][1] + (segment[1][1] - segment[0][1]) / 2;
        double[] midPoint = {midX, midY};

        return midPoint;
    }

    public static double[][] reverse(double[][] segment) {
        double reversedBeginX = segment[1][0];
        double reversedBeginY = segment[1][1];
        double reversedEndX = segment[0][0];
        double reversedEndY = segment[0][1];
        double[][] revercedSedment = {
                {reversedBeginX, reversedBeginY},
                {reversedEndX, reversedEndY}
        };

        return revercedSedment;
    }

    // Самостоятельная работа

    public static boolean isBelongToOneQuadrant(double[][] segment) {

        if((segment[0][0] > 0 && segment[0][1] > 0 && segment[1][0] > 0 && segment[1][1] > 0) ||
        (segment[0][0] < 0 && segment[0][1] < 0 && segment[1][0] > 0 && segment[1][1] > 0) ||
        (segment[0][0] < 0 && segment[0][1] < 0 && segment[1][0] < 0 && segment[1][1] < 0) ||
        (segment[0][0] > 0 && segment[0][1] > 0 && segment[1][0] < 0 && segment[1][1] < 0)) {
            return true;
        }

        return false;
    }
}
// END
