package exercise;

import org.w3c.dom.ls.LSOutput;

import java.util.Arrays;

class Point {
    public static int[] makePoint(int x, int y) {
        int[] point = {x, y};
        return point;
    }

    public static int getX(int[] point) {
        return point[0];
    }

    public static int getY(int[] point) {
        return point[1];
    }

    public static String pointToString(int[] point) {
        return String.format("(%d, %d)", point[0], point[1]);
    }

    public static int getQuadrant(int[] point) {
        if (point[0] == 0 || point[1] == 0) return 0;
        else if (point[0] > 0 && point[1] > 0) return 1;
        else if (point[0] < 0 && point[1] > 0) return 2;
        else if (point[0] < 0 && point[1] < 0) return 3;
        else if (point[0] > 0 && point[1] < 0) return 4;
        else return -1;
    }

    // Самостоятельная работа

    public static int[] getSymmetricalPoint (int[] point) {
        return makePoint(point[0], -1 * point[1]);
    }

    public static double calculateDistance(int[] point1, int[] point2) {
        return Math.sqrt(Math.pow(Math.abs(point1[0] - point2[0]), 2)
                + Math.pow(Math.abs(point1[1] - point2[1]), 2));
    }
    // END
}
