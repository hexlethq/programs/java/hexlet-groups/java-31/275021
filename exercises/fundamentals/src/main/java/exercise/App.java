package exercise;

class App {
    public static void numbers() {
        // BEGIN
        int a = 8;
        int b = 2;
        int c = 100;
        int d = 3;
        int result = a / b + c % d;
        System.out.println(result);
        // END
    }

    public static void strings() {
        String language = "Java";
        // BEGIN
        String action = "works on";
        String compiler = "JVM";
        String space = " ";
        String result = language + space + action + space + compiler;
        System.out.println(result);
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        // BEGIN
        String space = " ";
        String result = soldiersCount + space + name;
        System.out.println(result);
        // END
    }
}
